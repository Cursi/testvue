import { createApp } from 'vue'
import App from './App.vue'
import { createWebHistory, createRouter } from "vue-router";
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";

const app = createApp(App)
app.mount('#app')

const routes = [];
const router = createRouter({
    history: createWebHistory(),
    routes,
});
  
Sentry.init({
    app,
    dsn: "https://85af20437bdc4f40857566f75ab1d4ad@o1088891.ingest.sentry.io/6103758",
    integrations: [
      new Integrations.BrowserTracing({
        routingInstrumentation: Sentry.vueRouterInstrumentation(router),
        tracingOrigins: ["localhost", "testvue-dss-laborator-7-cursi.herokuapp.com", /^\//],
      }),
    ],
    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });